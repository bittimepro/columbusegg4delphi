// ***************************************************************************
//
// ColumbusEgg4Delphi
//
// Copyright (c) 2014-2019 Daniele Teti and bit Time Professionals Team
//
// https://bitbucket.org/bittimepro/columbusegg4delphi
//
//
// ***************************************************************************
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// ***************************************************************************

unit ColumbusUIListenerInterface;

interface

uses
{$IF CompilerVersion >= 20}
  System.UITypes,
{$IFEND}
  Dialogs;

type
  IColumbusUIListener = interface
    ['{D30889BC-EF38-4471-B364-B3104EC4F897}']
    function UIMessageDialog(const aMessage: string;
      const aDlgType: TMsgDlgType; const aButtons: TMsgDlgButtons; const aMessageCode: Integer = 0): Integer;
    function UIConfirm(const aMessage: String): Boolean;
    function UIInputBox(const ACaption, APrompt, ADefault: string): string;
  end;

implementation

end.
