unit MainFormU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, DB, ExtCtrls, DBCtrls,
  Grids, DBGrids, ColumbusModuleCustomersU, StdCtrls,
  ColumbusCommons, ADODB;

type
  TMainForm = class(TForm, IColumbusObserver)
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    DBNavigator1: TDBNavigator;
    Panel1: TPanel;
    lblItalianCustomer: TLabel;
    lblCaliforniaPeopleCount: TLabel;
    ADOQuery1: TADOQuery;
    ADOConnection1: TADOConnection;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FCustomerModule: TCustomerModule;
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure UpdateObserver(const Sender: TObject; const ModuleName: String);
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}


procedure TMainForm.FormCreate(Sender: TObject);
begin
  FCustomerModule := TCustomerModule.Create(ADOQuery1);
  FCustomerModule.RegisterObserver(Self);
  ADOQuery1.Open;
end;

procedure TMainForm.UpdateObserver(const Sender: TObject; const ModuleName: String);
begin
  lblCaliforniaPeopleCount.Caption := Format('%d persons lives in California',
    [(Sender as TCustomerModule).PeopleInCalifornia]);
  lblItalianCustomer.Visible := (Sender as TCustomerModule).IsItalianCustomer;
end;

end.
