object DataModuleMain: TDataModuleMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 356
  Width = 580
  object dsrcCustomers: TDataSource
    DataSet = dsCustomers
    Left = 224
    Top = 120
  end
  object ADOConnection1: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=Fireb' +
      'ird'
    LoginPrompt = False
    Left = 112
    Top = 120
  end
  object dsSales: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    DataSource = dsrcCustomers
    Parameters = <
      item
        Name = 'cust_no'
        DataType = ftInteger
        Precision = 10
        Value = 1001
      end>
    Prepared = True
    SQL.Strings = (
      'select * from sales where cust_no = :cust_no')
    Left = 144
    Top = 200
  end
  object dsCustomers: TADOQuery
    Connection = ADOConnection1
    CursorType = ctStatic
    Parameters = <>
    Prepared = True
    SQL.Strings = (
      'select * from customer')
    Left = 272
    Top = 160
    object dsCustomersCUST_NO: TIntegerField
      FieldName = 'CUST_NO'
    end
    object dsCustomersCUSTOMER: TStringField
      FieldName = 'CUSTOMER'
      Size = 25
    end
    object dsCustomersCONTACT_FIRST: TStringField
      FieldName = 'CONTACT_FIRST'
      Size = 15
    end
    object dsCustomersCONTACT_LAST: TStringField
      FieldName = 'CONTACT_LAST'
    end
    object dsCustomersPHONE_NO: TStringField
      FieldName = 'PHONE_NO'
    end
    object dsCustomersADDRESS_LINE1: TStringField
      FieldName = 'ADDRESS_LINE1'
      Size = 30
    end
    object dsCustomersADDRESS_LINE2: TStringField
      FieldName = 'ADDRESS_LINE2'
      Size = 30
    end
    object dsCustomersCITY: TStringField
      FieldName = 'CITY'
      Size = 25
    end
    object dsCustomersSTATE_PROVINCE: TStringField
      FieldName = 'STATE_PROVINCE'
      Size = 15
    end
    object dsCustomersCOUNTRY: TStringField
      FieldName = 'COUNTRY'
      Size = 15
    end
    object dsCustomersPOSTAL_CODE: TStringField
      FieldName = 'POSTAL_CODE'
      Size = 12
    end
    object dsCustomersON_HOLD: TStringField
      FieldName = 'ON_HOLD'
      FixedChar = True
      Size = 1
    end
  end
  object ADODataSet1: TADODataSet
    Active = True
    Connection = ADOConnection1
    CursorType = ctStatic
    CommandText = 'select * from CUSTOMER'
    Parameters = <>
    Left = 384
    Top = 200
  end
end
