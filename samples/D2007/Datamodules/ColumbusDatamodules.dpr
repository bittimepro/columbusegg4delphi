program ColumbusDatamodules;

uses
  Forms,
  ColumbusModuleCustomersU in 'ColumbusModuleCustomersU.pas',
  ColumbusModuleSalesU in 'ColumbusModuleSalesU.pas',
  ExportServiceU in 'ExportServiceU.pas',
  GeocodingServiceU in 'GeocodingServiceU.pas',
  MainDataModuleU in 'MainDataModuleU.pas' {DataModuleMain: TDataModule},
  MainFormU in 'MainFormU.pas' {MainForm},
  VclUIListener in 'VclUIListener.pas',
  ColumbusCommons in '..\..\..\ColumbusCommons.pas',
  ColumbusModulesLocator in '..\..\..\ColumbusModulesLocator.pas',
  ColumbusUIListenerInterface in '..\..\..\ColumbusUIListenerInterface.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDataModuleMain, DataModuleMain);
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
